"use strict"    

navigator.sayswho = (function(){
    var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

if (navigator.sayswho.indexOf("IE") + 1 || navigator.sayswho.indexOf("Edge") + 1) {
    $('.header-block').addClass("ie-header-block");
    $('.header-hyp').addClass("ie-header-hyp");
    $('.search').addClass("ie-search");
    $('.registration .submit-container p').addClass("ie-submit-paragraph");
};

if (navigator.sayswho.indexOf("Edge") + 1) {
    $('.header-hyp').addClass("edge-header-hyp");
    $('.checkmark').addClass("edge-checkmark");
    $('.search').removeClass("ie-search");
};

if (navigator.sayswho.indexOf("Safari") + 1) {
    $('.header-hyp').addClass("safari-header-hyp");
};

if (navigator.sayswho.indexOf("Mozilla") + 1) {
    $('.header-hyp').addClass("moz-header-hyp");
};

if (navigator.sayswho.indexOf("Chrome") + 1) {
    $('.search').addClass("ie-search");
};

if (navigator.sayswho.indexOf("Mozilla") + 1 || 
    navigator.sayswho.indexOf("Safari") + 1) {
    $('.header-hyp').addClass("cross-header-hyp");
};