'use strict'
$(function (){
    $(".device-menu").click(function(){
        $("div.header-block:nth-of-type(2)").slideToggle();
    });
});

$(function (){
    $('input').blur(function(){
        let tmpval = $(this).val();
        if(tmpval !== '') {
            $(this).addClass('with-value');
        }
    });
});

$(function (){
    $("#same-address-container").click(function(){
        if ($('#same-address').prop("checked")) {
            $("#same-address-inputs").slideUp();
        } else if (!$('#same-address').prop("checked")) {
            $("#same-address-inputs").slideDown();
        }
    })
});