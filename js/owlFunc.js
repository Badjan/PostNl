'use strict'

$('.owl-carousel').owlCarousel({
    loop:true,
    items: 1,
    margin: 10,
    responsiveClass:true,
    dots: true,
    dotsEach: true,
    dotsContainer: $(".owl-dots"),
    dotsSpeed: 1000,
    responsive:{
        0:{
            nav:false
        },
        600:{
            nav:false
        },
        1000:{
            nav:false
        }
    }
})

$( '.owl-dot' ).on( 'click', function() {
    $('.owl-carousel').trigger('to.owl.carousel', [$(this).index(), 300]);
    $( '.owl-dot' ).removeClass( 'active' );
    $(this).addClass( 'active' );
})